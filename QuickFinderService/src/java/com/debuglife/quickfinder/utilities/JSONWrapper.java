/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debuglife.quickfinder.utilities;

import org.json.simple.JSONObject;

/**
 *
 * @author mahbub
 */
public class JSONWrapper {
    public static JSONObject getJSONArray(Object value, String header){
        try{
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(header, value);
            return jSONObject;
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public static JSONObject getJSONObject(Object obj){
        try{
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("", obj);
            return jSONObject;
        }catch(Exception ex){
            System.err.println(ex.getMessage());
        }
        return null;
    }
}
