/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debuglife.quickfinder.controller;

import com.debuglife.quickfinder.utilities.CommonContents;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author mahbubhasan
 */
@ServerEndpoint("/socket")
public class SocketController {
    
    @OnOpen
    public void sessionCreate(Session session){
        try{
            String queryString = session.getQueryString();
            String[] params = queryString.split("=");
            if(CommonContents.socketMapping.containsKey(params[1])){
                CommonContents.socketMapping.remove(params[1]);
            }
            CommonContents.socketMapping.put(params[1], session);
        }catch(Exception ex){
            System.err.println(ex.getMessage());
        }
    }
    
    @OnMessage
    public void messageSend(String message){
        try{
            
        }catch(Exception ex){
            System.err.println(ex.getMessage());
        }
    }
    
    @OnClose
    public void sessionClose(){
        
    }
    
    @OnError
    public void sessionError(){
        
    }
}
