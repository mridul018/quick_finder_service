/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debuglife.quickfinder.dao;

import com.debuglife.quickfinder.pojo.User;

/**
 *
 * @author mhasan
 */
public interface UserDao {
    public boolean doRegistration(User user);
}
